fn=importdata('names.csv');%file names
gamma=zeros(length(fn), 1);
rho0=zeros(length(fn),1);
fileID=fopen('names.csv','r');
    parameters = fscanf(fileID, 'g=%frho0=%fsolution.mat\n',[2 Inf]);
    parameters  = parameters';
    gamma = parameters(:,1);
    rho0 = parameters(:,2);
fclose(fileID);
efr=zeros(length(fn), 1);
mass=zeros(length(fn), 1);
bindE=zeros(length(fn),1);
S = zeros(length(fn), 1);
handler=cell(1,1);
% inhandler=cell(length(fn),1);

for i=1:1:length(fn)%number of files
    
            load(char(fn(i)));
            % R=(sinh(Y(:,2))-Y(:,2)-2*((sinh(Y(:,2)/4)).^3)/3)/(4*pi);
			R=Y(:,1);
            r=T;
            efr(i)=r(end);
            hr=T(2)-T(1);
            mass(i)=dot(R,4*pi*r.*r)*hr;
	for cutoff=1:1:1
            hk=0.001;
			k=linspace(pi/efr(i),20,1000);
			k=k';
			hk=k(2)-k(1);
			
			K = (k.^(-1)*r').*sin(k*r')*hr;
			fft= K*R;%fourier mode
            
            fk=abs(fft);
% 			fk=fft.*fft;%modal fraction
            
% 			ftot=dot(fk,k.*k)
            
            %fkmax=(hr*dot(r.*r,R))^2;%the most weight at k=0
			fkmax=max(fk);
% 			fkmax=ftot; %normalize to 1
			
			fk=fk/fkmax;%first most weight
%         
 
			S(i,cutoff)=dot((fk.*log(fk)),(k.*k));
			S(i,cutoff)=-4*pi*hk*S(i,cutoff);
			%legends{cutoff,1} = strcat('Cutoff=', num2str((0.95+0.01*(cutoff-1))));
			
            %if (S(i)/St-1 <1E-3)
             %    break;
            %end
            %St = S(i);
			%plot(k,fk);
            disp(i);
%             if (i==1||(gamma(i)-gamma(i-1))~=0)
%                 save(strcat('gamma_',num2str(gamma(i)),'rho_',num2str(rho0(i)),'Cutoff=', num2str(cutoff),'.mat'),'fkmax','k','fft','fk','xi','theta','r','R');
%             end
    end
    
end
bindE=mass.*mass.*(3*gamma-4)./((5*gamma-6).*efr);

% prompt = 'What is the number of gammas?\n';
% result = input(prompt)
result=50;
Ni = result*1; %number of gamma
Nj=length(fn)/Ni;  % number of rho0
Entropy=zeros(Ni,Nj);
Gamma=zeros(Ni,Nj);
Efr=zeros(Ni,Nj);
Rho0=zeros(Ni,Nj);
Mass=zeros(Ni,Nj);
BindE=zeros(Ni,Nj)
for i=1:1:Ni
    for j=1:1:Nj
        Entropy(i,j)=S((i-1)*Ni+j,cutoff);
        Gamma(i,j)=gamma((i-1)*Ni+j);
        Efr(i,j)=efr((i-1)*Ni+j);
        Rho0(i,j)=rho0((i-1)*Ni+j);
        Mass(i,j)=mass((i-1)*Ni+j);
        BindE(i,j)=bindE((i-1)*Ni+j);
    end
end
save('results_nosquare.mat','Gamma','Entropy','Efr','Mass','Rho0','BindE','cutoff');
