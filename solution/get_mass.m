fn=importdata('names.csv');%file names

%variables allocation
Ntot = length(fn);
gamma=zeros(Ntot, 1);
rho0 = zeros(Ntot, 1);
efr=zeros(Ntot, 1);
mass=zeros(Ntot, 1);
bindE=zeros(Ntot,1);
alpha=zeros(Ntot,1);

% available variables initialization 
fileID=fopen('names.csv','r');
    parameters = fscanf(fileID, 'g=%frho0=%fsolution.mat\n',[2 Inf]);
    parameters  = parameters';
    gamma = parameters(:,1);
fclose(fileID);


for i=1:1:Ntot % number of files
    
            load(char(fn(i)));
            % R=(sinh(Y(:,2))-Y(:,2)-2*((sinh(Y(:,2)/4)).^3)/3)/(4*pi);
			R=Y(:,1); %rho(r)
            r=T; %r
            rho0(i)=R(1,1); %central density
            efr(i)=r(end);
            hr=T(2)-T(1);
            mass(i)=dot(R,4*pi*r.*r)*hr;
            alpha(i)=sqrt(gamma(i)/(gamma(i)-1))*((rho0(i))^(gamma(i)/2-1));
end

save('mass_bindE_efr','gamma','rho0','efr','mass','bindE','alpha');


Ni = input('Number of Rho0');
Nj=Ntot/Ni;  % number of rho0
Entropy=zeros(Ni,Nj);
Gamma=zeros(Ni,Nj);
Efr=zeros(Ni,Nj);
Rho0=zeros(Ni,Nj);
Mass=zeros(Ni,Nj);
BindE=zeros(Ni,Nj)
for i=1:1:Ni
    for j=1:1:Nj
        Entropy(i,j)=S((i-1)*Ni+j,cutoff);
        Gamma(i,j)=gamma((i-1)*Ni+j);
        Efr(i,j)=efr((i-1)*Ni+j);
        Rho0(i,j)=rho0((i-1)*Ni+j);
        Mass(i,j)=mass((i-1)*Ni+j);
        BindE(i,j)=bindE((i-1)*Ni+j);
    end
end














