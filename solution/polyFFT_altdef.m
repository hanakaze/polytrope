fn=importdata('names.csv');%file names
gamma=zeros(length(fn), 1);
 fileID=fopen('names.csv','r');
 gamma=fscanf(fileID,'%fsolution.mat');
 fclose(fileID);
efr=zeros(length(fn), 1);
mass=zeros(length(fn), 1);
bindE=zeros(length(fn),1);
S = zeros(length(fn), 1);
handler=cell(1,1);
% inhandler=cell(length(fn),1);


for i=1:length(fn)%number of files
    
            load(char(fn(i)));
            % R=(sinh(Y(:,2))-Y(:,2)-2*((sinh(Y(:,2)/4)).^3)/3)/(4*pi);
			R=Y(:,1);
            r=T;
            efr(i)=r(end);
            hr=T(2)-T(1);
            mass(i)=dot(R,4*pi*r.*r)*hr;
	for cutoff=1:1:1
            hk=0.001;
			k=linspace(pi/efr(i),5,100);
			k=k';
			hk=k(2)-k(1);
			
			K = (k.^(-1)*r').*sin(k*r')*hr;
			fft= K*R;%fourier mode
            
			fk=fft.*fft;%modal fraction
            
         
            ftot=4*pi*dot(fk,k.*k)*hk;
            fk=fk*((pi/efr(i))^3)/ftot;
         
            
% 			ftot=dot(fk,k.*k)
            
            %fkmax=(hr*dot(r.*r,R))^2;%the most weight at k=0
			fkmax=max(fk);
% 			fkmax=ftot; %normalize to 1
			
			% fk=fk/fk(1);%first most weight
%         
 
			S(i,cutoff)=dot((fk.*log(fk)),(k.*k));
			S(i,cutoff)=-4*pi*hk*S(i,cutoff);
			%legends{cutoff,1} = strcat('Cutoff=', num2str((0.95+0.01*(cutoff-1))));
			
            %if (S(i)/St-1 <1E-3)
             %    break;
            %end
            %St = S(i);
			%plot(k,fk);
			% save(strcat('gamma_',num2str(gamma(i)),'Cutoff=', num2str(cutoff),'.mat'),'k','fft','fk','xi','theta','r','R');
    end
    
end
% bindE=mass.*mass.*(3*gamma-4)./((5*gamma-6).*efr);
% save('results_altdef_T.mat','gamma','cutoff','S','efr','mass','bindE');
% handler{1,1} = figure;
% plot(gamma,S),xlabel('gamma'),ylabel('Entropy');
% saveas(handler{1,1},'phiS_altdef_T.fig');
