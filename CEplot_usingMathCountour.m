surf(Gamma(:,1),Rho0(1,:),Entropy); 
lengends=cell(level,1);
 hold on;
 cc=hsv(level)
 for i=1:1:level
     h(i)=plot3(CE(i).X,CE(i).Y,CE(i).Z,'LineWidth',3,'color',cc(i,:));
     legends{i,1}=(strcat('Mass Level ',num2str(i)));
 end
 legend(h,legends)
 xlabel('Gamma');ylabel('Rho0');zlabel('Entropy');