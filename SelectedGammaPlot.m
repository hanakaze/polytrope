plot(Rho0(1,:),Entropy(1,:)./Rho0(1,:),'LineWidth',3);
hold on;
line([1,2.96],[0.654,0.654],'LineWidth',3);
plot(Rho0(16,:),Entropy(16,:)./Rho0(16,:),'LineWidth',3);
legend('\gamma = 1.25','\gamma = 4/3','\gamma = 1.40');
title('S\rho_c^{-1} v.s. \rho_{c} for Various \gamma','FontSize',18);
xlabel('\rho_c','FontSize',14);
ylabel('S\rho_c{-1}','FontSize',14);
