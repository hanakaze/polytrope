fn=importdata('names_varyg.csv');%file names

%variables allocation
Ntot = length(fn);
gamma=zeros(Ntot, 1);
rho0 = zeros(Ntot, 1);
efr=zeros(Ntot, 1);
mass=zeros(Ntot, 1);
bindE=zeros(Ntot,1);
S = zeros(Ntot, 1);
alpha=zeros(Ntot,1);

% available variables initialization 
fileID=fopen('names_varyg.csv','r');
    parameters = fscanf(fileID, 'g=%frho0=%fsolution.mat\n',[2 Inf]);
    parameters  = parameters';
    gamma = parameters(:,1);
fclose(fileID);
    
% calculation requst input
cutoff = [1.0];
Ncut = length(cutoff);

%preparation for calculation
S = zeros(Ntot/length(gamma),length(gamma),Ncut);

for i=1:1:Ntot % number of files
    
            load(char(fn(i)));
            % R=(sinh(Y(:,2))-Y(:,2)-2*((sinh(Y(:,2)/4)).^3)/3)/(4*pi);
			R=Y(:,1); %rho(r)
            r=T; %r
            rho0(i)=R(1,1); %central density
            efr(i)=r(end);
            hr=T(2)-T(1);
            mass(i)=dot(R,4*pi*r.*r)*hr;
            alpha(i)=sqrt(gamma(i)/(gamma(i)-1))*((rho0(i))^(gamma(i)/2-1));

	for j=1:1:Ncut
			k=linspace(pi/(cutoff(j)*efr(i)),20,5000);
			k=k';
			hk=k(2)-k(1);
			
			K = (k.^(-1)*r').*sin(k*r')*hr;
			fft= K*R;%fourier mode
            
            % fk=abs(fft);
			fk=fft.*fft;    %modal fraction
            
% 			ftot=dot(fk,k.*k)
            
            %fkmax=(hr*dot(r.*r,R))^2;%the most weight at k=0
			fkmax=max(fk);
% 			fkmax=ftot; %normalize to 1
			
			fk=fk/fkmax;%first most weight
%         
 
			S(i,j)=dot((fk.*log(fk)),(k.*k));
			S(i,j)=-4*pi*hk*S(i,j);
            
			save(strcat('gamma',num2str(gamma(i)),'rho_',num2str(rho0(i)),'Cutoff=',num2str(cutoff(j)),'.mat'),'fkmax','fk','k','fft','r','R');
    end
    
end


% Nk=Ntot/Nk;  % number of rho0
% Entropy=zeros(Ni,Nj);
% Gamma=zeros(Ni,Nj);
% Efr=zeros(Ni,Nj);
% Rho0=zeros(Ni,Nj);
% Mass=zeros(Ni,Nj);
% BindE=zeros(Ni,Nj)
% for i=1:1:Ni
%     for j=1:1:Nj
%         Entropy(i,j)=S((i-1)*Ni+j,cutoff);
%         Gamma(i,j)=gamma((i-1)*Ni+j);
%         Efr(i,j)=efr((i-1)*Ni+j);
%         Rho0(i,j)=rho0((i-1)*Ni+j);
%         Mass(i,j)=mass((i-1)*Ni+j);
%         BindE(i,j)=bindE((i-1)*Ni+j);
%     end
% end
% S(gamma,cutoff)

% S(i,j) corresponds to gamma(i), rho0(i), cutoff(j)
bindE=mass.*mass.*(3*gamma-4)./((5*gamma-6).*efr);
save('results_varyg.mat','S','efr','gamma','cutoff','mass','bindE','rho0','alpha');
quit;

