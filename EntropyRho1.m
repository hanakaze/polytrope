level=5;
Mass = Entropy./Rho0;
CTMass=contourcs(Mass,level);
field1 = 'Level';  value1 = zeros(length(CTMass),1);
field2 = 'X';  value3 = 1.0;
field3 = 'Y';  value4 = 1.0;
field4 = 'Z';  value2 = 1.0;
CE = struct(field1,value1,field2,value2,field3,value3,field4,value4);


Entropy = Entropy./Rho0;

for i=1:1:length(CTMass)
        CE(i).Level=i;
    for j=1:1:length(CTMass(i).X)
        CE(i).X(j)=interp1(Gamma(:,1),CTMass(i).X(j));
        CE(i).Y(j)=interp1(Rho0(1,:),CTMass(i).Y(j));
        CE(i).Z(j)=interp2(Entropy,CTMass(i).X(j),CTMass(i).Y(j));
    end
end

cc=hsv(level);

 for i=1:1:level
     h(i)=plot3(CE(i).X,CE(i).Y,CE(i).Z,'LineWidth',3,'color',cc(i,:));
     legends{i,1}=(strcat('Mass Level ',num2str(i)));
 end
 legend(h,legends)
 xlabel('Gamma');ylabel('Rho0');zlabel('Entropy');