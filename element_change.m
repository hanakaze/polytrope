NEntropy=zeros(50,50);
for i=1:1:50
    for j=1:1:50
        alpha(i,j)=sqrt(Gamma(i,j)/(Gamma(i,j)-1))*((Rho0(i,j)^(Gamma(i,j)/2-1/2)));
        NEntropy(i,j)=Entropy(i,j)*(alpha(i,j)^3);
    end
end
surf(Gamma(:,1),Rho0(1,:),NEntropy);
xlabel('Gamma');ylabel('Rho0');zlabel('Entropy');
