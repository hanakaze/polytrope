function dy = polyrunge(r,y,k)
dy=zeros(2,1);
dy(1) = y(2);
dy(2) = -2*y(2)/r-y(1)^(1/(k-1));
end