for i=1:1:length(S(:,1))
    S(i,:)=S(i,:)*(alpha(i)^3);
    S(i,:)=smooth(S(i,:));
end

NEntropy=S;
hold on;
N=50;
cc=hsv(N);
legends=cell(N,1);
startIn=1;
for i=1:1:N
% 
%     plot(Rho0(i,:),NEntropy(i,:));
%     [val,ind]=findpeaks(-NEntropy(i,:));
%     plot(Rho0(i,ind),-val,'o');
%      [val,ind]=findpeaks(NEntropy(i,:));
%     plot(Rho0(i,ind),val,'X');
%     
%   plot(Gamma(:,i),NEntropy(:,i));
%     [val,ind]=findpeaks(-NEntropy(:,i));
%     plot(Gamma(ind,i),-val,'o');
%      [val,ind]=findpeaks(NEntropy(:,i));
%     plot(Gamma(ind,i),val,'X');
   
    h(i)=plot(gamma,NEntropy(:,i+startIn),'color',cc(i,:),'LineWidth',2);
    legends{i,1}=strcat('\pi/(',num2str(cutoff(i+startIn),'%.2f'),'R)');
    [val,ind]=findpeaks(-NEntropy(:,i+startIn));
    plot(gamma(ind),-val,'o','MarkerSize',6,'MarkerFaceColor','auto');
     [val,ind]=findpeaks(NEntropy(:,i+startIn));
    plot(gamma(ind),val,'d','MarkerSize',6,'MarkerFaceColor','auto');

end
hleg=legend(h,legends);
line([4/3,4/3],[4,6],'LineWidth',2,'LineStyle','-.');
line([5/3,5/3],[4,6],'LineWidth',2,'LineStyle','-.');
xlabel('\gamma','FontSize',16);
ylabel('S\alpha^3','FontSize',16,'Rotation',0);
title('S\alpha^3 vs. \gamma with Various Cutoffs','FontSize',20)
% htitle = get(hleg,'Title');
% set(htitle,'String','\pi/k_{min}R');
