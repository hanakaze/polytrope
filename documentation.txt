Entropy: 
	original definition

Revised-Entropy/CE:
	multiply by fft^2 (fk)

Non-square:
	using fft in modal fraction rather than fft^2 (fk)

g:
	gamma

bindE:
	binding energy

a3:
	alpha^3


Documents:

element_change:
	used to change every element of a matrix;

bundle:
	a bunch of curves in one graph

FFT:
	general FFT calculation code

cuttest:
	cutoff of radius test

test_templete:
	open for all temporary use


