names = importdata('names.csv');
hold on;
N= numel(names);
cc=hsv(N)
legends=cell(N,1);
for i=1:1:N
    load(names{i});
    h(i)=plot(k,fk,'--','LineWidth',1.5,'color',cc(i,:));xlim([0,3]);
    %h(i)=plot(k,k.*k.*fk,'--','LineWidth',1.5,'color',cc(i,:));
%     xlim([2,3]);
    legends{i,1}=strtok(names{i,1}, 'rho');
    [pv,pi]=max(fk);
    plot(k(pi),pv,'v','color',cc(i,:),'LineWidth',5);
end
legend(h,legends{:,1});

    