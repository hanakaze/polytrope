a = 0.0;
b = 30.0;
N = 300000; %number of steps
h = (b-a)/N;
options = odeset('MaxStep',h,'AbsTol',[1e-10 1e-10]);
r = linspace(h, b, N);
theta=zeros(1,N);
xi=zeros(1,N);
M=70;% number of gamma
P=1;% number of rho0
gamma=zeros(M,1);
rho0=zeros(P,1) 
names=cell(M,1);

for i=1:1:M % indice of gamma
    gamma(i)=1.65+0.001*(i-1);
    k=gamma(i);
    [T Y] = ode45(@(r,y) polyVrunge(r,y,k),r,[1 0],options);
    ind=find(imag(Y(:,1))~=0 | real(Y(:,1)) < 1e-10);
    T=T(1:(ind(1)-1));
    Y=Y(1:(ind(1)-1),:);
    xi=T; % xi
    theta=Y; % Y(:,1) is theta, Y(:,2) is d(theta)/d(xi)
    for j=1:1:P% indice of rho0 
        rho0(j)=1.0+0.04*(j-1);
        T=(rho0(j)^(gamma(i)/2-1))*sqrt(gamma(i)/(gamma(i)-1))*xi; % r = alpha*xi
        Y(:,1)=rho0(j)*(theta(:,1).^(1/(gamma(i)-1)));  % rho(r) 
%         inhandler=figure;
%         plot(T,Y(:,1),'r-',T,Yana,'g--');
%         legend('numerical','anaylitical');
%         names{i,1}=[strcat(num2str(gamma(i),'%0.3f'),'solution.mat')];
        save(strcat('g=',num2str(gamma(i),'%0.3f'),'rho0=',num2str(rho0(j),'%0.3f'),'solution.mat'),'xi','theta','T','Y');
    end
end


